import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { AppRoutingModule } from './app.routing.module';
import {AbstractService} from './AbstractService';
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
	UserComponent
  ],
  imports: [
    BrowserModule,
	AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AbstractService],
  bootstrap: [AppComponent]
})
export class AppModule { }
