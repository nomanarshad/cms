import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../models/user.model';

import { AbstractService } from '../AbstractService';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styles: []
})
export class UserComponent implements OnInit {

  user: User = new User();
  users: User[];
  objUser : any;

  constructor(private router: Router, private abstractService: AbstractService) {

  }

  ngOnInit() {
    this.abstractService.getUsers()
      .subscribe( data => {
        this.users = data;
      });
  };


  createUser(): void {
    this.abstractService.createUser(this.user ,  "users")
        .subscribe( data => {
          alert("User created successfully.");
        });

  };
  
 

  addUser() {
    this.objUser = {};
  }


  deleteUser(user: User): void {
    this.abstractService.deleteUser("users", user)
      .subscribe( data => {
        this.users = this.users.filter(u => u !== user);
      })
  };

}


