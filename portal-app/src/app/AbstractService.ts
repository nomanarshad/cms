import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User } from './models/user.model';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AbstractService {

  constructor(private http:HttpClient) {}

  private userUrl = 'http://localhost:8080/';
  //private userUrl = '/api';

  public getUsers() {
    return this.http.get<User[]>(this.userUrl +"users" );
  }

  public deleteUser(endPoint,user) {
    return this.http.delete(this.userUrl + endPoint+ "/"+ user.id);
  }

  public createUser(user , endpoint) {
    return this.http.post<User>(this.userUrl +endpoint , user);
  }

}
