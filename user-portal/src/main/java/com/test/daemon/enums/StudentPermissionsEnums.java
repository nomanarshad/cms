package com.test.daemon.enums;

public enum StudentPermissionsEnums {
    
	/** View permission with mask 2 raise power 1*/
	VIEW(2, "View"),


	;

	/**
	 * Instantiates a new permission matrix.
	 *
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	StudentPermissionsEnums(int key, String value) {
		this.key = key;
		this.value = value;
	}

	/** key of the constant */
	private int key;

	/** value against the constant key */
	private String value;

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Gets the key by value.
	 *
	 * @param matrexValue the matrex value
	 * @return the key by value
	 */
	public static int getKeyByValue(String matrexValue) {
		int key = -1;
		for (StudentPermissionsEnums permissionMatrix : values()) {
			if (permissionMatrix.getValue().contains(matrexValue)) {
				key = permissionMatrix.getKey();
				break;
			}
		}
		return key;
	}

	/**
	 * Gets the value by key.
	 *
	 * @param matrixKey the matrix key
	 * @return the value by key
	 */
	public static String getValueByKey(int matrixKey) {
		String value = "";
		for (StudentPermissionsEnums permissionMatrix : values()) {
			if (permissionMatrix.getKey() == matrixKey) {
				value = permissionMatrix.getValue();
				break;
			}
		}
		return value;
	}
}
