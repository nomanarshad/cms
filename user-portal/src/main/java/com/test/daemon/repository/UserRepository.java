package com.test.daemon.repository;


import java.util.List;

import org.springframework.data.repository.Repository;

import com.test.daemon.entity.UserEntity;
public interface UserRepository extends Repository<UserEntity, Integer> {

    void delete(UserEntity user);

    List<UserEntity> findAll();

    UserEntity findById(int id);

    UserEntity save(UserEntity user);
}
