package com.test.daemon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import com.test.daemon.entity.RolesEntity;

public interface RoleRepository extends JpaRepository<RolesEntity, Integer> {

	RolesEntity findById(int id);

	RolesEntity save(RolesEntity role);
	
	long countByRoleName(String Name);
}
