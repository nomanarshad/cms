package com.test.daemon.dao;

import com.test.daemon.abstracthibernate.dao.IGenericDao;
import com.test.daemon.entity.UserEntity;

public interface UserDao  extends IGenericDao< UserEntity >{

}
