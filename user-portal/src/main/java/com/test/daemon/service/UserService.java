package com.test.daemon.service;

import java.util.List;

import com.test.daemon.entity.UserEntity;

public interface UserService {

	UserEntity create(UserEntity user);

	UserEntity delete(int id);

    List<UserEntity> findAll();

    UserEntity findById(int id);
    
    UserEntity update(UserEntity user);
}
