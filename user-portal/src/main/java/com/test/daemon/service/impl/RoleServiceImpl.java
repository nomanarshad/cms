package com.test.daemon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.daemon.entity.RolesEntity;
import com.test.daemon.repository.RoleRepository;
import com.test.daemon.service.RoleService;

@Service
public class RoleServiceImpl  implements RoleService{

	
    
    @Autowired
    private RoleRepository roleRepository;
    
    
	@Override
	public RolesEntity create(RolesEntity role) {
		return roleRepository.save(role);
	}

	@Override
	public RolesEntity delete(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RolesEntity> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RolesEntity findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RolesEntity update(RolesEntity role) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long countByRoleName(String Name) {
		return roleRepository.countByRoleName(Name);
	}
}
