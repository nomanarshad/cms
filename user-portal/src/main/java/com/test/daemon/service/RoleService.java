package com.test.daemon.service;

import java.util.List;

import com.test.daemon.entity.RolesEntity;

public interface RoleService {
	
	RolesEntity create(RolesEntity role);

	RolesEntity delete(int id);

    List<RolesEntity> findAll();

    RolesEntity findById(int id);
    
    RolesEntity update(RolesEntity role);

    long countByRoleName(String Name);
}
