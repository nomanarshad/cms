package com.test.daemon.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.daemon.entity.UserEntity;
import com.test.daemon.repository.UserRepository;
import com.test.daemon.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;
    
    @Override
    public UserEntity create(UserEntity user) {
        return repository.save(user);
    }

    @Override
    public UserEntity delete(int id) {
    	UserEntity user = findById(id);
        if(user != null){
            repository.delete(user);
        }
        return user;
    }

    @Override
    public List<UserEntity> findAll() {
       return repository.findAll();
    }

    @Override
    public UserEntity findById(int id) {
        return repository.findById(id);
    }

    @Override
    public UserEntity update(UserEntity user) {
        return repository.save(user);
    }
}
