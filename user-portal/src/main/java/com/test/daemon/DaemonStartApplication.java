package com.test.daemon;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import com.test.daemon.entity.RolesEntity;
import com.test.daemon.entity.UserEntity;
import com.test.daemon.service.RoleService;
import com.test.daemon.service.UserService;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class DaemonStartApplication implements CommandLineRunner{

	
	@Autowired
	private UserService userService;
	
	@Autowired
	RoleService roleService;
	
	public static void main(String[] args) {
		SpringApplication.run(DaemonStartApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		long count = roleService.countByRoleName("Admin");
		if(count <= 0){
			RolesEntity rolesEntity = new RolesEntity( "Admin", "Admin", true, true, true, true, true);
			System.out.println( "init : role intialized" + rolesEntity.toString() );
			RolesEntity	role = roleService.create(rolesEntity);
		
			UserEntity user = new UserEntity( "Admin", "Admin", "admin@gmail.com", "nomanarshad20@gmail.com", "Admin", role);
			user.setPassword("admin");
			userService.create(user);
			System.out.println( "init : user created" + role.toString() );
		}else{
			System.out.println( "init : role count :" + count );
		}
	}
}
