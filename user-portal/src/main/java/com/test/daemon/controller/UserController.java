package com.test.daemon.controller;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.daemon.entity.UserEntity;
import com.test.daemon.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping({ "/users" })
public class UserController {

	final static Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	

	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST )
	public ResponseEntity<Object> login(@RequestBody UserEntity user) {
		System.out.println("Person create " + user.toString());
		user = userService.create(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	

	@RequestMapping(value = "", method = RequestMethod.POST )
	public ResponseEntity<Object> create(@RequestBody UserEntity user) {
		System.out.println("Person create " + user.toString());
		user = userService.create(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET )
	public ResponseEntity<Object> findOne(@PathVariable("id") int id) {
		UserEntity user = userService.findById( id);
		System.out.println("Person Find " + user.toString());
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT )
	public ResponseEntity<Object> update(@PathVariable("id") int id, @RequestBody UserEntity user) {
		user.setId(id);
		userService.update(user);
		System.out.println("Person update " + user.toString());
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE )
	public ResponseEntity<Object> delete(@PathVariable("id") int id) {
		UserEntity user = userService.delete(id);
		System.out.println("Person update " + user.toString());
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.GET )
	public ResponseEntity<Object> findAll() {
		List<UserEntity> user = userService.findAll();
		System.out.println("Person update " + user.toString());
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

}
