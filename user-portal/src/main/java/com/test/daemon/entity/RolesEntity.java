package com.test.daemon.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class RolesEntity {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String roleName;

	@Column
	private String roleType;

	@Column
	private boolean create;

	@Column
	private boolean read;

	@Column
	private boolean update;

	@Column
	private boolean delete;

	@Column
	private boolean restore;

	public RolesEntity() {

	}

	public RolesEntity( String roleName, String roleType, boolean create, boolean read, boolean update,
			boolean delete, boolean restore) {
		super();
		this.roleName = roleName;
		this.roleType = roleType;
		this.create = create;
		this.read = read;
		this.update = update;
		this.delete = delete;
		this.restore = restore;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public boolean isCreate() {
		return create;
	}

	public void setCreate(boolean create) {
		this.create = create;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public boolean isRestore() {
		return restore;
	}

	public void setRestore(boolean restore) {
		this.restore = restore;
	}

	@Override
	public String toString() {
		return "RolesEntity [id=" + id + ", roleName=" + roleName + ", roleType=" + roleType + ", create=" + create
				+ ", read=" + read + ", update=" + update + ", delete=" + delete + ", restore=" + restore + "]";
	}



}
