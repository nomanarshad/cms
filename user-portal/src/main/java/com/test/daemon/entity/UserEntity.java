package com.test.daemon.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column
    private String firstName;
    
    @Column
    private String lastName;
    
    @Column(name="email", unique=true, nullable=false)
    private String email;
    
    @Column
    private String password;
  

	@Column
    private String contact;
    
    @Column
    private String address;

    /** The meta data RolesEntity. */
    @OneToOne ( fetch = FetchType.EAGER )
    private RolesEntity rolesEntity;

    
    public UserEntity(){
    	
    }
    
	public UserEntity( String firstName, String lastName, String email, String contact, String address,
			RolesEntity rolesEntity) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.contact = contact;
		this.address = address;
		this.rolesEntity = rolesEntity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public RolesEntity getRolesEntity() {
		return rolesEntity;
	}

	public void setRolesEntity(RolesEntity rolesEntity) {
		this.rolesEntity = rolesEntity;
	}
    
	  
    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}
